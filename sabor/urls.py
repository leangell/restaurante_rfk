from django.urls import path
from rest_framework import routers
from sabor.views import ListWaiter, DetailWaiter, AddWaiter, WaiterUpdateView, DeleteWaiter, ListClient, DetailClient, \
    AddClient, DeleteClient, ClientUpdateView, DeleteTable, TableUpdateView, AddTable, DetailTable, ListTable, \
    DeleteProduct, ProductUpdateView, AddProduct, DetailProduct, ListProduct, DeleteInvoice, InvoiceUpdateView, \
    AddInvoice, DetailInvoice, ListInvoice, ListOrder, DetailOrder, AddOrder, OrderUpdateView, DeleteOrder, \
    ClientViewSet, WaiterViewSet, TableViewSet, ProductViewSet, InvoiceViewSet, OrderViewSet

router = routers.DefaultRouter()
router.register(r'client', ClientViewSet)
router.register(r'waiter', WaiterViewSet)
router.register(r'table', TableViewSet)
router.register(r'product', ProductViewSet)
router.register(r'invoice', InvoiceViewSet)
router.register(r'order', OrderViewSet)

urlpatterns = [
    path('listwaiter', ListWaiter.as_view(), name='listwaiter'),
    path('detail_waiter/<int:pk>/', DetailWaiter.as_view(), name='detail_waiter'),
    path('create_waiter', AddWaiter.as_view(), name='create_waiter'),
    path('update_waiter/<int:pk>/', WaiterUpdateView.as_view(), name='update_waiter'),
    path('delete_waiter/<int:pk>/', DeleteWaiter.as_view(), name='delete_waiter'),

    path('listclient', ListClient.as_view(), name='listclient'),
    path('detail_client/<int:pk>/', DetailClient.as_view(), name='detail_client'),
    path('create_client', AddClient.as_view(), name='create_client'),
    path('update_client/<int:pk>/', ClientUpdateView.as_view(), name='update_client'),
    path('delete_client/<int:pk>/', DeleteClient.as_view(), name='delete_client'),

    path('listtable', ListTable.as_view(), name='listtable'),
    path('detail_table/<int:pk>/', DetailTable.as_view(), name='detail_table'),
    path('create_table', AddTable.as_view(), name='create_table'),
    path('update_table/<int:pk>/', TableUpdateView.as_view(), name='update_table'),
    path('delete_table/<int:pk>/', DeleteTable.as_view(), name='delete_table'),

    path('listproduct', ListProduct.as_view(), name='listproduct'),
    path('detail_product/<int:pk>/', DetailProduct.as_view(), name='detail_product'),
    path('create_product', AddProduct.as_view(), name='create_product'),
    path('update_product/<int:pk>/', ProductUpdateView.as_view(), name='update_product'),
    path('delete_product/<int:pk>/', DeleteProduct.as_view(), name='delete_product'),

    path('listinvoice', ListInvoice.as_view(), name='listinvoice'),
    path('detail_invoice/<int:pk>/', DetailInvoice.as_view(), name='detail_invoice'),
    path('create_invoice', AddInvoice.as_view(), name='create_invoice'),
    path('update_invoice/<int:pk>/', InvoiceUpdateView.as_view(), name='update_invoice'),
    path('delete_invoice/<int:pk>/', DeleteInvoice.as_view(), name='delete_invoice'),

    path('listorder', ListOrder.as_view(), name='listorder'),
    path('detail_order/<int:pk>/', DetailOrder.as_view(), name='detail_order'),
    path('create_order', AddOrder.as_view(), name='create_order'),
    path('update_order/<int:pk>/', OrderUpdateView.as_view(), name='update_order'),
    path('delete_order/<int:pk>/', DeleteOrder.as_view(), name='delete_order'),
]