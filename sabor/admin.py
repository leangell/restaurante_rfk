from django.contrib import admin

from .models import Product, Order, Table, Waiter, Client,Invoice

# Register your models here.


@admin.register(Client)
class ClientAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'first_name',
        'last_name',
        'observations'
    )


@admin.register(Waiter)
class WaiterAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'first_name',
        'last_name'
    )


@admin.register(Table)
class TableAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'number_diners',
        'location'
    )


@admin.register(Product)
class ProductAdmin(admin.ModelAdmin):
    list_display = (
        'name',
        'description',
        'imported',

    )


@admin.register(Invoice)
class InvoiceAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'date',
        'client',
        'waiter',
        'table',

    )
    list_editable = (
        'date',
        'client',
        'waiter',
        'table',
    )


@admin.register(Order)
class OrderAdmin(admin.ModelAdmin):
    list_display = (
        'quantity',
        'products',
        'invoices',
    )