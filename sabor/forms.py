from django import forms

from sabor.models import Client, Waiter, Table, Product, Invoice, Order


class FormClient(forms.ModelForm):
    class Meta:
        model = Client
        fields = (
            'first_name',
            'last_name',
            'observations',
        )


class FormWaiter(forms.ModelForm):
    class Meta:
        model = Waiter
        fields = (
            'first_name',
            'last_name',
        )


class FormTable(forms.ModelForm):
    class Meta:
        model = Table
        fields = (
            'number_diners',
            'location',
        )


class FormProduct(forms.ModelForm):
    class Meta:
        model = Product
        fields = (
            'name',
            'imported',
            'description',
        )


class FormInvoice(forms.ModelForm):
    date = forms.DateField(widget=forms.SelectDateWidget)
    client = forms.ModelChoiceField(queryset=Client.objects.all())
    waiter = forms.ModelChoiceField(queryset=Waiter.objects.all())
    table = forms.ModelChoiceField(queryset=Table.objects.all())

    class Meta:
        model = Invoice
        fields = (
            'date',
            'client',
            'waiter',
            'table',
        )


class FormOrder(forms.ModelForm):
    quantity = forms.IntegerField(widget=forms.NumberInput(attrs={'class': 'form-control'}))
    products = forms.ModelChoiceField(queryset=Product.objects.all())
    invoices = forms.ModelChoiceField(queryset=Invoice.objects.all())

    class Meta:
        model = Order
        fields = (
            'quantity',
            'products',
            'invoices',
        )
