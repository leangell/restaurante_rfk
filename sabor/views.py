from .forms import FormWaiter, FormClient, FormTable, FormProduct, FormInvoice, FormOrder
from .models import Waiter, Client, Table, Product, Invoice, Order
from django.urls import reverse_lazy
from django.views.generic import (UpdateView, ListView, CreateView, DeleteView, DetailView)
from rest_framework import viewsets

# Create your views here.
from .serializers import ClientSerializer, WaiterSerializer, TableSerializer, ProductSerializer, InvoiceSerializer, \
    OrderSerializer


class ListWaiter(ListView):
    model = Waiter
    template_name = 'waiter/listwaiter.html'
    ordering = 'id'


class DetailWaiter(DetailView):
    model = Waiter
    template_name = 'waiter/detail_waiter.html'


class AddWaiter(CreateView):
    model = Waiter
    form_class = FormWaiter
    template_name = 'waiter/create_waiter.html'
    success_url = reverse_lazy('listwaiter')


class DeleteWaiter(DeleteView):
    model = Waiter
    template_name = 'waiter/delete_waiter.html'
    success_url = reverse_lazy('listwaiter')


class WaiterUpdateView(UpdateView):
    model = Waiter
    template_name = 'waiter/create_waiter.html'
    fields = ['first_name', 'last_name']
    success_url = reverse_lazy('listwaiter')


class ListClient(ListView):
    model = Client
    template_name = 'cliente/listclient.html'
    ordering = 'id'


class DetailClient(DetailView):
    model = Client
    template_name = 'cliente/detail_client.html'


class AddClient(CreateView):
    model = Client
    form_class = FormClient
    template_name = 'cliente/create_client.html'
    success_url = reverse_lazy('listclient')


class DeleteClient(DeleteView):
    model = Client
    template_name = 'cliente/delete_client.html'
    success_url = reverse_lazy('listclient')


class ClientUpdateView(UpdateView):
    model = Client
    template_name = 'cliente/create_client.html'
    fields = ['first_name', 'last_name', 'observations']
    success_url = reverse_lazy('listclient')


# ---------------------------------

class ListTable(ListView):
    model = Table
    template_name = 'table/listtable.html'
    ordering = 'id'


class DetailTable(DetailView):
    model = Table
    template_name = 'table/detail_table.html'


class AddTable(CreateView):
    model = Table
    form_class = FormTable
    template_name = 'table/create_table.html'
    success_url = reverse_lazy('listtable')


class DeleteTable(DeleteView):
    model = Table
    template_name = 'table/delete_table.html'
    success_url = reverse_lazy('listtable')


class TableUpdateView(UpdateView):
    model = Table
    template_name = 'table/create_table.html'
    fields = ['number_diners', 'location']
    success_url = reverse_lazy('listtable')


# --------------------------------------------------

class ListProduct(ListView):
    model = Product
    template_name = 'product/listproduct.html'
    ordering = 'id'


class DetailProduct(DetailView):
    model = Product
    template_name = 'product/detail_product.html'


class AddProduct(CreateView):
    model = Product
    form_class = FormProduct
    template_name = 'product/create_product.html'
    success_url = reverse_lazy('listproduct')


class DeleteProduct(DeleteView):
    model = Product
    template_name = 'product/delete_product.html'
    success_url = reverse_lazy('listproduct')


class ProductUpdateView(UpdateView):
    model = Product
    template_name = 'product/create_product.html'
    fields = ['name', 'imported', 'description']
    success_url = reverse_lazy('listproduct')


# -----------------------------------------------------

class ListInvoice(ListView):
    model = Invoice
    template_name = 'invoice/listinvoice.html'
    ordering = 'id'


class DetailInvoice(DetailView):
    model = Invoice
    template_name = 'invoice/detail_invoice.html'


class AddInvoice(CreateView):
    model = Invoice
    form_class = FormInvoice
    template_name = 'invoice/create_invoice.html'
    success_url = reverse_lazy('listinvoice')


class DeleteInvoice(DeleteView):
    model = Invoice
    template_name = 'invoice/delete_invoice.html'
    success_url = reverse_lazy('listinvoice')


class InvoiceUpdateView(UpdateView):
    model = Invoice
    template_name = 'invoice/create_invoice.html'
    fields = ['date', 'client', 'waiter', 'table']
    success_url = reverse_lazy('listinvoice')


# ------------------------------------------------------

class ListOrder(ListView):
    model = Order
    template_name = 'order/listorder.html'
    ordering = 'id'


class DetailOrder(DetailView):
    model = Order
    template_name = 'order/detail_order.html'


class AddOrder(CreateView):
    model = Order
    form_class = FormOrder
    template_name = 'order/create_order.html'
    success_url = reverse_lazy('listorder')


class DeleteOrder(DeleteView):
    model = Order
    template_name = 'order/delete_order.html'
    success_url = reverse_lazy('listorder')


class OrderUpdateView(UpdateView):
    model = Order
    template_name = 'order/create_order.html'
    fields = ['quantity', 'products', 'invoices']
    success_url = reverse_lazy('listorder')


class ClientViewSet(viewsets.ModelViewSet):
    queryset = Client.objects.all()
    serializer_class = ClientSerializer
    # permission_classes = [permissions.IsAuthenticated]


class WaiterViewSet(viewsets.ModelViewSet):
    queryset = Waiter.objects.all()
    serializer_class = WaiterSerializer


class TableViewSet(viewsets.ModelViewSet):
    queryset = Table.objects.all()
    serializer_class = TableSerializer


class ProductViewSet(viewsets.ModelViewSet):
    queryset = Product.objects.all()
    serializer_class = ProductSerializer


class InvoiceViewSet(viewsets.ModelViewSet):
    queryset = Invoice.objects.all()
    serializer_class = InvoiceSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
