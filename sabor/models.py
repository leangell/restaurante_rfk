from xml.parsers.expat import model
from django.db import models

# Create your models here.
class Client(models.Model):
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)
    observations = models.CharField(max_length=225)

    def __str__(self):
        return f'{self.first_name}  {self.last_name}'

class Waiter(models.Model):
    first_name = models.CharField(max_length=45)
    last_name = models.CharField(max_length=45)

    def __str__(self):
        return self.last_name

class Table(models.Model):
    number_diners = models.PositiveSmallIntegerField()
    location = models.CharField(max_length=45)

    def __str__(self):
        return self.location


class Product(models.Model):
    name = models.CharField(max_length=45)
    imported = models.FloatField()
    description = models.CharField(max_length=45)

    def __str__(self):
        return self.name



class Invoice(models.Model):
    date = models.DateField()
    client = models.ForeignKey(Client, on_delete=models.CASCADE)
    waiter = models.ForeignKey(Waiter, on_delete=models.CASCADE)
    table = models.ForeignKey(Table, on_delete=models.CASCADE)
    def __str__(self):
        return f'{self.date} {self.waiter}'
    

class Order(models.Model):
    quantity = models.IntegerField()
    products = models.ForeignKey(Product, on_delete=models.CASCADE)
    invoices = models.ForeignKey(Invoice, on_delete=models.CASCADE )

    def __str__(self):
        return f'{self.products} {self.invoices}'