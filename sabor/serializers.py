from rest_framework.serializers import ModelSerializer, HyperlinkedModelSerializer
from .models import Order, Client, Waiter, Table, Product, Invoice


class ClientSerializer(ModelSerializer):
    class Meta:
        model = Client
        fields = ['id', 'first_name', 'last_name', 'observations']


class WaiterSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Waiter
        fields = ['url', 'first_name', 'last_name']


class TableSerializer(ModelSerializer):
    class Meta:
        model = Table
        fields = '__all__'


class ProductSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'


class InvoiceSerializer(ModelSerializer):
    class Meta:
        model = Invoice
        fields = ['date', 'client', 'waiter', 'table']


class OrderSerializer(HyperlinkedModelSerializer):
    class Meta:
        model = Order
        fields = ['url', 'quantity', 'products', 'invoices']
